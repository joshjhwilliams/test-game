import {combineReducers} from 'redux';
import gameState from './logicReducer'

const rootReducer = combineReducers({
   gameState: gameState
});

export default rootReducer;
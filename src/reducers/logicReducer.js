import {
    GAME_START,
    COUNTDOWN,
    MOVE_CHARACTER_ISLAND, MOVE_CHARACTER_BOAT,
    MOVE_CHARACTER_SAFE
} from "../actions/characterActions"
import initialState from './initialState';

export default function gameState(state = initialState,action){
    switch(action.type){
        case COUNTDOWN:
            state = gameRules(state);

            if(state.seconds <= 0){

                return {
                    ...state,
                    seconds: 0,
                    gameStatusMessage: 'Game over :( you\'ve run out of time.',
                    gameStatus: 'Game over!',
                    charactersIsland: [],
                    characterBoat: [],
                    charactersSafe: []
                }

            }else{
            return {
                ...state,
                seconds: state.seconds -1
            }}
        case MOVE_CHARACTER_ISLAND:
            if(state.characterBoat.length === 0){


                    return {
                        ...state,
                        charactersIsland: state.charactersIsland.filter(character => character !== action.payload),
                        characterBoat: state.charactersIsland.filter(character => character === action.payload)
                    }
            }else if(state.characterBoat.length !== 0){

                let charcs = state.charactersIsland.concat(state.characterBoat);

                charcs = charcs.filter(character => character !== action.payload);


                return {
                    ...state,
                    characterBoat: state.charactersIsland.filter(character => character === action.payload),
                    charactersIsland: charcs

                }

            }
        case MOVE_CHARACTER_BOAT:
            if(state.charactersSafe.length === 0){
                return {
                    ...state,
                    charactersSafe: state.characterBoat.filter(character => character === action.payload),
                    characterBoat: []
                }
            }else if(state.charactersSafe.length !== 0){
                let charcs = state.charactersSafe.concat(state.characterBoat);
                return {
                    ...state,
                    charactersSafe: charcs,
                    characterBoat: []
                }
            }
        case MOVE_CHARACTER_SAFE:

            if(state.characterBoat.length === 0) {

                return {
                    ...state,
                    charactersSafe: state.charactersSafe.filter(character => character !== action.payload),
                    characterBoat: state.charactersSafe.filter(character => character === action.payload)
                }

            }else if(state.characterBoat.length !== 0) {

                let charcs = state.charactersSafe.concat(state.characterBoat)
                charcs = charcs.filter(character => character !== action.payload);




                return {
                    ...state,
                    characterBoat: state.charactersSafe.filter(character => character === action.payload),
                    charactersSafe: charcs


                }

            }
        case GAME_START:
            let newState = initialState
            return  {
                ...newState,
                gameStatus: 'playing',
                gameStatusMessage: '',

            }

        default:
            return state;
    }



  function gameRules(state)  {
        let charsIsland = state.charactersIsland;
        let charsSafe = state.charactersSafe;

        let zombieIsland = charsIsland.some(character => (character.name ==='zombie'));
        let civilianIsland = charsIsland.some(character => (character.name ==='citizen'));
        let soldierIsland = charsIsland.some(character => (character.name ==='soldier'));

        let zombieSafe= charsSafe.some(character => (character.name ==='zombie'));
        let civilianSafe = charsSafe.some(character => (character.name === 'citizen'));
        let soldierSafe = charsSafe.some(character => (character.name === 'soldier'));

        if( (zombieIsland && civilianIsland) && !soldierIsland){
            return {
                ...state,
                gameStatus: 'Game over!',
                gameStatusMessage: 'The zombie has eaten the civilian',
                charactersIsland: [],
                characterBoat: [],
                charactersSafe: []
            }

        }else if( (zombieIsland && soldierIsland) && !civilianIsland){
            return {
                ...state,
                gameStatus: 'Game over!',
                gameStatusMessage: 'The soldier has killed the zombie',
                charactersIsland: [],
                characterBoat: [],
                charactersSafe: []
            }
        }else if ( (zombieSafe && civilianSafe) && !soldierSafe){
            return {
                ...state,
                gameStatus: 'Game over!',
                gameStatusMessage: 'The zombie has killed the civilian',
                charactersIsland: [],
                characterBoat: [],
                charactersSafe: []
            }
        }else if ( (zombieSafe && soldierSafe) && !civilianSafe){
            return {
                ...state,
                gameStatus: 'Game over!',
                gameStatusMessage: 'The soldier has killed the zombie',
                charactersIsland: [],
                characterBoat: [],
                charactersSafe: []
            }
        }else if(charsSafe.length === 3){

            return {
                ...state,
                gameStatus: 'Winner!',
                gameStatusMessage: 'Congrats you have won!'
            }

        }else{
            return {
                ...state
            }
        }


  }



}







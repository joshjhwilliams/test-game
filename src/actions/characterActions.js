
export const GAME_START = 'GAME_START';
export const COUNTDOWN = 'COUNTDOWN';
export const MOVE_CHARACTER_ISLAND = 'MOVE_CHARACTER_ISLAND';
export const MOVE_CHARACTER_BOAT = 'MOVE_CHARACTER_BOAT';
export const MOVE_CHARACTER_SAFE ='MOVE_CHARACTER_SAFE'



export function moveCharacter(character,state){
    return {
        type: MOVE_CHARACTER_ISLAND,
        payload: character,
        state: state
    }
}


export function moveCharacterBoat(character,state){
    return {
        type: MOVE_CHARACTER_BOAT,
        payload: character,
        state: state
    }
}

export function moveCharacterSafe(character,state){
    return {
        type: MOVE_CHARACTER_SAFE,
        payload: character,
        state: state
    }
}

export function startGame(){
    return {
        type: GAME_START
    }
}

export function countdown(state){

    return {
        type:  COUNTDOWN,
        state: state
    }


}
import React, { Component } from 'react';
import './App.css';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {countdown,moveCharacter,moveCharacterBoat,moveCharacterSafe,startGame} from "./actions/characterActions";
import Tooltip from "react-simple-tooltip"


class App extends Component {

    constructor(props){
        super(props)

    }

    tick() {


     // send the props through to an action that fires a countdown method?
        if((this.props.gameStatus !== "Winner!" && this.props.gameStatus !== "Game over!") && this.props.gameStatus !== "start") {
            this.props.countdown(this.props.completeState)
        }

    }

    componentDidMount() {
        this.interval = setInterval(() => this.tick(), 1000);
    }

    componentWillUnmount() {
        clearInterval(this.interval);

    }

    renderTimer() {
        if ((this.props.gameStatus !== "Winner!" && this.props.gameStatus !== "Game over!") && this.props.gameStatus !== "start") {

            return <div className="float-right">Time left: {this.props.seconds} seconds</div>
        } else if(this.props.gameStatus === 'start'){

                return <div className='mb-1'>
                    <div className='pt-2 mb-1' style={{height:40 + '%'}}>
                        <p>{this.props.gameStatusMessage}</p>
                    </div>
                    <button onClick={() =>this.props.startGame()} type="button" className="btn btn-primary" style={{width: 15 +'%'}}>Start</button>
                </div>

        }
        else{
                return <div className='mb-1'>
                    <div className='alert-danger pt-2 mb-1' style={{height:40 + '%'}}>
                    <p>{this.props.gameStatusMessage}</p>
                    </div>
                    <button onClick={() =>this.props.startGame()} type="button" className="btn btn-primary" style={{width: 15 +'%'}}>Play Again?</button>
                </div>

            }
        }


    renderCharListIsland() {
        if (this.props.gameStatus !== 'start') {
            return this.props.charactersIsland.map((char) => {
                return (
                    <Tooltip content="Click to move character to boat">
                        <li className="list-group-item rounded-pill mb-1"
                            onClick={() => this.props.moveCharacter(char, this.props.completeState)}
                            key={char.name}>{char.name}</li>
                    </Tooltip>
                )

            });
        }else{
            return this.props.charactersIsland.map((char) => {
                return (
                        <li className="list-group-item rounded-pill mb-1"
                            key={char.name}>{char.name}</li>
                )

            });
        }
    }

    renderCharListBoat(){
        return this.props.characterBoat.map((char) =>{
            return(
                <Tooltip content="Click to move to shore">
                <li className="list-group-item rounded-pill mb-1" onClick={() =>this.props.moveCharacterBoat(char,this.props.completeState)} key={char.name} >{char.name}</li>
                </Tooltip>
            )

        });
    }

    renderCharListSafe(){
        return this.props.charactersSafe.map((char) =>{
            return(
                <Tooltip content="Click to move back to boat">
                <li className="list-group-item rounded-pill mb-1" onClick={() =>this.props.moveCharacterSafe(char,this.props.completeState)} key={char.name} >{char.name}</li>
                </Tooltip>
            )

        });
    }


    render() {

        return(
            <div>
                <div className="container text-center pt-3">
                    <h1>Zombie, Citizen, Soldier</h1>
                    <p>The objective of the game is to move all three characters from the island across to the shore via the boat within the time limit. The boat can only carry one character at once.</p>
                    <p><strong>There are two rules:</strong> <ul style={{listStylePosition: 'inside'}}><li>The citizen cannot be left alone with the zombie or they will be eaten</li><li>The soldier cannot be left alone with the zombie or they will kill it.</li></ul></p>
                    {this.renderTimer()}

                    <div className="row">


                <div className="col-md-4 border border-secondary">
                <ul className="list-group">

                    <h3>Island</h3>

                    {this.renderCharListIsland()}


                </ul>
                </div>
                        <div className="col-md-4 border border-secondary">
                <ul className="list-group">
                    <h3>Boat</h3>

                    {this.renderCharListBoat()}
                </ul>
                        </div>

                    <div className="col-md-4 border border-secondary">
                        <ul className="list-group">
                    <h3>Shore</h3>

                    {this.renderCharListSafe()}
                </ul>
                    </div>
                    </div>
                </div>

            </div>
        )
    }



}



function mapStateToProps(state) {

    return {
        completeState: state.gameState,
        seconds: state.gameState.seconds,
        charactersIsland: state.gameState.charactersIsland,
        charactersSafe: state.gameState.charactersSafe,
        characterBoat: state.gameState.characterBoat,
        gameStatus: state.gameState.gameStatus,
        gameStatusMessage: state.gameState.gameStatusMessage

    };


}

 function mapDispatchToProps(dispatch) {
    return {
       countdown: bindActionCreators(countdown,dispatch),
        moveCharacter: bindActionCreators(moveCharacter,dispatch),
        moveCharacterBoat: bindActionCreators(moveCharacterBoat,dispatch),
        moveCharacterSafe: bindActionCreators(moveCharacterSafe,dispatch),
        startGame: bindActionCreators(startGame,dispatch)
    }


}





export default connect(mapStateToProps,mapDispatchToProps)(App);
